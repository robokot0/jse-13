package com.nlmkit.korshunov_am.tm.controller;

import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.enumerated.Role;
import com.nlmkit.korshunov_am.tm.service.ProjectService;

/**
 * Контроллер проектов
 */
public class ProjectController extends AbstractController {

    /**
     * Сервис проектов
     */
    private  final ProjectService projectService;

    /**
     * Конструктов
     * @param projectService Сервис проектов
     */
    public ProjectController(ProjectService projectService) {
       this.projectService = projectService;
    }

    /**
     * Создать проект
     * @return 0 создано
     */
    public int createProject(){
        System.out.println("[CREATE PROJECT]");
        if (!this.testAuthUser())return 0;
        System.out.println("Please enter project name: ");
        final String name = scanner.nextLine();
        System.out.println("Please enter project description: ");
        final String description = scanner.nextLine();
        projectService.create(name,description,this.getUser().getId());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Изменить проект
     * @param project Проект
     * @return 0 изменено
     */
    public  int updateProject(final Project project){
        System.out.println("Please enter project name: ");
        final String name = scanner.nextLine();
        System.out.println("Please enter project description: ");
        final String description = scanner.nextLine();
        projectService.update(project.getId(),name,description,project.getUserId());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Обновить проект по индексу
     * @return 0 обновлено
     */
    public int updateProjectByIndex(){
        System.out.println("[UPDATE PROJECT BY INDEX]");
        if (!this.testAuthUser())return 0;
        System.out.println("Please enter project index: ");
        final int index = Integer.parseInt(scanner.nextLine())-1;
        final Project project = this.getUser().getRole()==Role.ADMIN ?
                projectService.findByIndex(index) :projectService.findByIndex(index,this.getUser().getId());
        if (project == null) System.out.println("[FAIL]");
        else updateProject(project);
        return 0;
    }

    /**
     * Удалить проект по имени
     * @return 0 выполнено
     */
    public int removeProjectByName(){
        System.out.println("[REMOVE PROJECT BY NAME]");
        if (!this.testAuthUser())return 0;
        System.out.println("Please enter project name: ");
        final String name = scanner.nextLine();
        final Project project = this.getUser().getRole()==Role.ADMIN ?
                projectService.removeByName(name):projectService.removeByName(name,this.getUser().getId());
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    /**
     * Удалить проект по ID
     * @return 0 выоплнено
     */
    public int removeProjectByID(){
        System.out.println("[REMOVE PROJECT BY ID]");
        if (!this.testAuthUser())return 0;
        System.out.println("Please enter project ID: ");
        final long id = scanner.nextLong();
        final Project project = this.getUser().getRole()==Role.ADMIN ?
                projectService.removeById(id):projectService.removeById(id,this.getUser().getId());
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    /**
     * Удалить проект по индексу
     * @return 0 выполнено
     */
    public int removeProjectByIndex(){
        System.out.println("[REMOVE PROJECT BY INDEX]");
        if (!this.testAuthUser())return 0;
        System.out.println("Please enter project index: ");
        final int index = scanner.nextInt()-1;
        final Project project = this.getUser().getRole()==Role.ADMIN ?
                projectService.removeByIndex(index):projectService.removeByIndex(index,this.getUser().getId());
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    /**
     * Удалить все проекты
     * @return 0 выполнено
     */
    public int clearProject(){
        System.out.println("[CLEAR PROJECT]");
        if (!this.testAuthUser())return 0;
        if (this.getUser().getRole()==Role.ADMIN)  projectService.clear();
        else projectService.clear(this.getUser().getId());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Вывести список проектов
     * @return 0 выполнено
     */
    public int listProject(){
        System.out.println("[LIST PROJECT]");
        if (!this.testAuthUser())return 0;
        int index = 1;
        for (final Project project:
                this.getUser().getRole()==Role.ADMIN ?
                projectService.findAll():
                projectService.findAll(this.getUser().getId())
        ) {
            System.out.println(index + ". " + project.getId()+ ": " + project.getName());
            index ++;
        }
        System.out.println("[OK]");
        return 0;
    }

    /**
     * ВЫвеси информацию по проекту
     * @param project Проект
     */
    public void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[VIEW PROJET]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
    }

    /**
     * Вывести информацию по проекту по индексу
     * @return 0 выполнено
     */
    public int viewProjectByIndex() {
        if (!this.testAuthUser())return 0;
        System.out.println("Enter, project index:");
        final  int index = scanner.nextInt() - 1;
        final  Project project = this.getUser().getRole()==Role.ADMIN ? projectService.findByIndex(index) : projectService.findByIndex(index,this.getUser().getId());
        viewProject(project);
        return 0;
    }
    /**
     * Поменять ИД пользователя в проекте проект искать по ИД
     * @param user Пользователь на ид которого менять
     * @return 0 выполнено
     */
    public int setProjectUserById(final User user){
        if (!this.testAdminUser())return 0;
        System.out.println("Please enter project ID: ");
        final long projectId = scanner.nextLong();
        final Project project = projectService.findById(projectId);
        if(project==null)return 0;
        projectService.update(project.getId(),project.getName(),project.getDescription(),user.getId());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Поменять ИД пользователя в проекте проект искать по индексу
     * @param user Пользователь на ид которого менять
     * @return 0 выполнено
     */
    public int setProjectUserByIndex(final User user){
        if (!this.testAdminUser())return 0;
        System.out.println("Please enter project index: ");
        final int index = scanner.nextInt()-1;
        final Project project = projectService.findByIndex(index);
        if(project==null)return 0;
        projectService.update(project.getId(),project.getName(),project.getDescription(),user.getId());
        System.out.println("[OK]");
        return 0;
    }

}
