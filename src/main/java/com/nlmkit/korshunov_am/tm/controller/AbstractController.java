package com.nlmkit.korshunov_am.tm.controller;

import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.enumerated.Role;

import java.util.Scanner;

/**
 * Базовый абстрактный класс для контроллеров
 */
public abstract class AbstractController {
    /**
     * объект для ввода комманд
     */
    protected final Scanner scanner = new Scanner(System.in);

    /**
     * Текущий пользователь
     */
    private User user=null;

    /**
     * Установить текущего пользователя
     * @return текущий пользователь
     */
    public User getUser() {
        return user;
    }
    /**
     * Получить текущего пользователя
     * @param user пользователь
     */
    public void setUser(User user) {
        this.user = user;
    }
    /**
     * Проверка что пользователь аутентифицировался
     * @return true пользовтаель аутентифицировался
     * пользователь не аутентифицировался
     */
    public boolean testAuthUser(){
        if (this.user==null){
            System.out.println("Please auth: ");
            System.out.println("[FAIL]");
            return false;
        }
        return true;
    }

    /**
     * Проверка что пользователь аутентифицировался и это администратор
     * @return true пользовтаель аутентифицировался и это администратор false
     * пользователь не аутентифицировался или это не администратор
     */
    public boolean testAdminUser(){
        if(!testAuthUser())
            return false;
        if (this.user.getRole() != Role.ADMIN){
            System.out.println("Please auth admin user: ");
            System.out.println("[FAIL]");
            return false;
        }
        return true;
    }

}
